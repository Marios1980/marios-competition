# Competition Project
22 oktober 2018

Jef Inghelbrecht

Eerste examenopdracht Programmeren 4
1. Maak een workspace met de naam voornaam-competition
2. Maak een betekenisvolle README pagina
2. Maak het model
3. Maak de views en de css
4. Maak de controller, voorlopig met een switch statement

## Support & Documentation

1. Voor de __leerstof__ zie [Modern Ways Programmeren 4](https://modernways.be/myap/it/school/course/Programmeren%204.html) 
2. Voor **markdown** syntaxis zie [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
3. Als je mij repo wilt binnenhalen en jouw code overschrijven:
    1. git fetch origin master
    2. git reset --hard origin/master
4. Force git push to overwrite remote files
    1. You should be able to force your local revision to the remote repo by using
    2. git push -f remote branch
## Stappen
1. composer.json
2. run composer dumpautoload
3. maak de model klassen voor ModernWays\Competition
4. maak de views voor Modernays\Competition
5. maak de controller voor ModernWays\Competition
6. [Installing Composer on OS X ](https://www.abeautifulsite.net/installing-composer-on-os-x)
