<?php
require ('../config.php');
require ('../common.php');

if (isset($_POST['submit'])) {
    // anders dan Tania, wij escapen in het begin zodat
    // de waarde al 'veilig' is vooraleer naar de database te sturen
    $new_user = array(
    	"firstname" => escape($_POST['firstname']),
    	"lastname"  => escape($_POST['lastname']),
    	"email"     => escape($_POST['email']),
    	"age"       => escape($_POST['age']),
    	"location"  => escape($_POST['location'])
    );

    // volgende regel gebuik je op te debuggen (manuele breakpoint)
    // print_r($new_user);
    // niet veilige manier, gevoelig voor SQL injectie
    // $insertSql = "insert into users (firstname, lastname, email) values ('{$new_user['firstname']}', '{$new_user['lastname']}', '{$new_user['email']}')";
    // echo $insertSql;

    // nu met parameters, is veilig
    $insertSql = "insert into users (firstname, lastname, email, age, location) values (:firstname, :lastname, :email, :age, :location)";
    // echo $insertSql;

    try {
        $connection = new \PDO($host, $user, $password, $options);
        $statement = $connection->prepare($insertSql);
        $statement->execute($new_user);
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }
}

include ('template/header.php'); 
?>
<div id="feedback">
<?php if (isset($_POST['submit']) && $statement) {
    echo $new_user['firstname'] . ' ' . $new_user['lastname'] . ' is toegevoegd';
}
?>

</div>
<!-- form>(label+input:text)*6 -->
<form action="" method="post">
    <div>
        <label for="firstname">Voornaam</label>
        <input type="text" name="firstname" id="firstname">
    </div>
    <div>
        <label for="lastname">Familienaam</label>
        <input type="text" name="lastname" id="lastname">
    </div>
    <div>
        <label for="email">E-mail</label>
        <input type="text" name="email" id="email">
    </div>
    <div>
        <label for="age">Leeftijd</label>
        <input type="text" name="age" id="age">
    </div>
    <div>
        <label for="location">Plaats</label>
        <input type="text" name="location" id="location">
    </div>
    <input type="submit" value="Verzenden" name="submit">
</form>
    
<?php include ('template/footer.php'); ?>
