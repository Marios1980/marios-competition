<?php
include "../config.php";
include "../common.php";

if (isset($_GET["id"])) {
  try {
    $connection = new \PDO($host, $user, $password, $options);
  
    $id = $_GET["id"];

    $sql = "DELETE FROM users WHERE id = :id";

    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $success = "User successfully deleted";
  } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }
}




 try {
        $connection = new \PDO($host, $user, $password, $options);
        
        $sql= "SELECT * from users";
        $statement = $connection->prepare($sql);
        //$statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        echo 'aantal rijen ' . $statement->rowCount();
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }                           


?>

<?php include ('template/header.php'); ?>


<h2>Delete users</h2>

<?php  
if ($success) echo $success;
?>
		<h2>Results</h2>

		<table>
			<thead>
				<tr>
					<th>#</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email Address</th>
					<th>Age</th>
					<th>Location</th>
					<th>Date</th>
					<th>Delete</th>

				</tr>
			</thead>
			<tbody>
	<?php foreach ($result as $row) { 
	?>
			<tr>
				<td><?php echo escape($row["id"]); ?></td>
				<td><?php echo escape($row["firstname"]); ?></td>
				<td><?php echo escape($row["lastname"]); ?></td>
				<td><?php echo escape($row["email"]); ?></td>
				<td><?php echo escape($row["age"]); ?></td>
				<td><?php echo escape($row["location"]); ?></td>
				<td><?php echo escape($row["date"]); ?> </td>
				<td><a href="delete.php?id=<?php echo escape($row["id"]); ?>">Delete</a></td>

			</tr>
		<?php } ?> 
			</tbody>
	</table>

    
<?php include ('template/footer.php'); ?>