<?php 
include ('../config.php');
include ('../common.php');

// volgende mag alleen worden uitgevoerd als er op
// de submit knop is geklikt

    try {
        $connection = new \PDO($host, $user, $password, $options);
        
        $sqlSelect = "SELECT * from users";
        $statement = $connection->prepare($sqlSelect);
        //$statement->bindParam(':location', $location, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        echo 'aantal rijen ' . $statement->rowCount();
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }                           



include ('template/header.php'); ?>

<h2>Toon iedereen</h2>
<!-- form>(label+input:text)*6 -->


<?php  
if ($result && $statement->rowCount() > 0) { 
?>
		<h2>Results</h2>

		<table>
			<thead>
				<tr>
					<th>#</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email Address</th>
					<th>Age</th>
					<th>Location</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach ($result as $row) { 
	?>
			<tr>
				<td><?php echo escape($row["id"]); ?></td>
				<td><?php echo escape($row["firstname"]); ?></td>
				<td><?php echo escape($row["lastname"]); ?></td>
				<td><?php echo escape($row["email"]); ?></td>
				<td><?php echo escape($row["age"]); ?></td>
				<td><?php echo escape($row["location"]); ?></td>
				<td><?php echo escape($row["date"]); ?> </td>
			</tr>
		<?php } ?> 
			</tbody>
	</table>
	<?php } else { ?>
		<blockquote>Niemand gevonden. <?php echo $location; ?>.</blockquote>
	<?php } 
 ?> 
    
<?php include ('template/footer.php'); ?>