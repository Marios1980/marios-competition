<?php
include('../common.php');
include('../config.php');

if (isset($_POST['submit'])) {
    // anders dan Tania, wij escapen in het begin zodat
    // de waarde al 'veilig' is vooraleer naar de database te sturen
    $new_user = array(
        "id" => escape($_POST['id']),
    	"firstname" => escape($_POST['firstname']),
    	"lastname"  => escape($_POST['lastname']),
    	"email"     => escape($_POST['email']),
    	"age"       => escape($_POST['age']),
    	"location"  => escape($_POST['location'])
    );

    // volgende regel gebuik je op te debuggen (manuele breakpoint)
    // print_r($new_user);
    // niet veilige manier, gevoelig voor SQL injectie
    // $insertSql = "insert into users (firstname, lastname, email) values ('{$new_user['firstname']}', '{$new_user['lastname']}', '{$new_user['email']}')";
    // echo $insertSql;

    // nu met parameters, is veilig
    $updateSql = "update users set firstname = :firstname, lastname = :lastname, email = :email, age = :age, location = :location WHERE id = :id";
    echo $updateSql;

    try {
        $connection = new \PDO($host, $user, $password, $options);
        $statement = $connection->prepare($updateSql);
        $statement->execute($new_user);
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }
}


if (isset($_GET['id'])) {
    $id = escape($_GET['id']);
    try {
        $connection = new \PDO($host, $user, $password, $options);
         
        $sqlSelect = "SELECT * from users WHERE id = :id";
        $statement = $connection->prepare($sqlSelect);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        echo 'aantal rijen ' . $statement->rowCount();
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    } 
} else {
    echo 'er ging iets verkeerd';
}

include ('template/header.php');
?>
<h2>Update <?php echo escape($result[0]['firstname']) . ' ' . escape($result[0]['lastname']);?></h2>
<div id="feedback">
<?php if (isset($_POST['submit']) && $statement) {
    echo $new_user['firstname'] . ' ' . $new_user['lastname'] . ' is gewijzigd';
} ?>
</div>
<!-- form>(label+input:text)*6 -->
<form action="" method="post">
    <input type="text" name="id" id="id" value="<?php echo escape($result[0]['id']);?>">
    
    <div>
        <label for="firstname">Voornaam</label>
        <input type="text" name="firstname" id="firstname" value="<?php echo escape($result[0]['firstname']);?>">
    </div>
    <div>
        <label for="lastname">Familienaam</label>
        <input type="text" name="lastname" id="lastname" value="<?php echo escape($result[0]['lastname']);?>">
    </div>
    <div>
        <label for="email">E-mail</label>
        <input type="text" name="email" id="email" value="<?php echo escape($result[0]['email']);?>">
    </div>
    <div>
        <label for="age">Leeftijd</label>
        <input type="text" name="age" id="age" value="<?php echo escape($result[0]['age']);?>">
    </div>
    <div>
        <label for="location">Plaats</label>
        <input type="text" name="location" id="location" value="<?php echo escape($result[0]['location']);?>">
    </div>
    <input type="submit" value="Verzenden" name="submit">
</form>
<h3>$_GET:</h3>
<pre>
    <?php 
        var_dump($_GET); 
    ?>
</pre>
<h3>$_POST:</h3>
<pre>
    <?php 
        var_dump($_POST); 
    ?>
</pre>
<?php
    include('template/footer.php');
?>